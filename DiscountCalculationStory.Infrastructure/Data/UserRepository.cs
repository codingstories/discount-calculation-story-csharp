namespace DiscountCalculationStory.Infrastructure.Data
{
    using System.Linq;
    using DiscountCalculationStory.Interfaces;
    using DiscountCalculationStory.Models;

    public class UserRepository : IUserRepository
    {
        public virtual User GetUserById(int userId)
        {
            using var db = new UserContext();

            var user = db.Users.Find(userId);

            return MapToUser(user);

            User MapToUser(Entities.User user) =>
                new() { Orders = user.Orders.Select(MapToOrder).ToList() };

            Order MapToOrder(Entities.Order order) =>
                new() { OrderPrice = order.OrderPrice, TotalTax = order.TotalTax };
        }
    }
}
