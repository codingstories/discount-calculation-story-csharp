# Discount Calculation Story

**To read**: <https://codingstories.io/story/https:%2F%2Fgitlab.com%2Fcodingstories%2Fdiscount-calculation-story-csharp>

**Estimated reading time**: 45 min

## Story Outline

Read a small story about calculation of a discount during the online purchase.

This story is about violations of clean design principles with focus on
Dependency Inversion Principle (D in SOLID).

## Story Organization

**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_design, #csharp, #dependency_inversion_principle
