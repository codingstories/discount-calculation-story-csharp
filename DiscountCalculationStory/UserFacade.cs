namespace DiscountCalculationStory
{
    using DiscountCalculationStory.Interfaces;

    public class UserFacade
    {
        private readonly IUserRepository userRepository;

        public UserFacade(IUserRepository userRepository) =>
            this.userRepository = userRepository;

        public decimal GetDiscountForUserById(int userId)
        {
            var user = this.userRepository.GetUserById(userId);
            return user.CalculateDiscount();
        }
    }
}
